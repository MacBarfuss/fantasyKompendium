.. Fantasy Kompendium documentation master file, created by
   sphinx-quickstart on Sat Nov 14 13:45:02 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Fantasy Kompendium's documentation!
==============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Hintergründe <purpose/index>
   basics/index
   creations

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. This is my structure for headlines in my documents:
   ### over and under
         and with indentation of 5 characters
         and at least 40 characters long and 5 characters after last letter of caption
   === over and under
         and with indentation of 5 characters
         and at least 40 characters long and 5 characters after last letter of caption
   ### at least 20 characters long and 5 characters after last letter of caption
   === at least 20 characters long and 5 characters after last letter of caption
   """ with same size as caption
   --- with same size as caption
   ... with same size as caption
