########################################
     Geografie (Land und Natur)
########################################

Beschreibung der Landschaft und der Natur

Geologie
    Entwicklungsgeschichte der Region

Topografie
    Die Topografie ist jenes Teilgebiet der Kartografie, das sich mit der detaillierten Vermessung, Darstellung und
    Beschreibung der Weltenoberfläche und der mit ihr fest verbundenen natürlichen und künstlichen Objekte (Situation)
    befasst. Die größte Bedeutung haben das Gelände und seine Formen (Relief), die Gewässer, die Bodennutzung bzw. der
    Bewuchs, sowie die Bauwerke und Verkehrswege.

Astronomie
    Die Wissenschaft von den Gestirnen untersucht mit naturwissenschaftlichen Mitteln die Positionen, Bewegungen und Eigenschaften der Objekte am Himmel.

Klima
    Klima, Jahreszeiten und typisches Wetter

Ressourcen
    Gesteine, Metalle, ...

Pflanzenwelt
    Flora

Tierwelt
    Fauna

Bewohner
    Spezifische Merkmale der Einheimischen

    Hierbei sollte nicht auf die Kultur der Einheimischen eingegangen werden, da es dazu eigene Kapitel gibt.

Gefahren
    bestimmte für die Region spezielle Krankheiten und Gefahren

Magische Orte
    auch über die Grenzen bekannte Magische Orte, bzw. Beschreibung von Orten, die an anderer Stelle referenziert
    werden.

Magische Tiere
    Tiere mit magischen Fähigkeiten

Artefakte
    Gegenstände mit magischen Fähigkeiten