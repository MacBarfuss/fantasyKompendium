########################################
     Vorlagen
########################################

.. toctree::
    :hidden:

    animal
    culture
    knowledge
    landscape
    nation
    person
    plant
