########################################
    Der Kosmos
########################################

========================================
    Die Membran
========================================

Die besiedelbare Welt besteht aus einer Membran, die kein Ende kennt. Sie trennt die Oberwelt von der Unterwelt. Die Membran ist nicht glatt, sondern schlägt unregelmäßig Wellen. Eine normale Welle hat einen Radius von mindestens 1000 km, so dass die Krümmung nicht sichtbar ist und beim Zeichnen von Karten vernachlässigt werden können. Die Schwerkraft wirkt immer lotrecht zur Membran hin. Dies gilt sowohl für die Bewohner der Obewelt, als auch die Bewohner der Unterwelt. Aus Sicht einer Welt steht also die andere Welt Kopf.

Für den normalen Umgang mit Karten hat die Krümmung der Membran keine relevante Bedeutung. Sie liefert lediglich eine Erklärungsmöglichkeit für sonst nicht erklärbare Phänomene.

Die Bewohner sowohl der Oberwelt, als auch der Unterwelt, wissen in der Regel nichts vom Aufbau des Kosmos. Gelangt ein Bewohner in das Wissen, so wird ihn das vor große Rätsel stellen und möglicherweise wird er daran brechen. Den Göttern ist der Aufbau aber sehr wohl bekannt. Während die Oberwelt die Domäne des Äthers ist, besteht die Unterwelt aus Kontra-Äther. Der Aufenthalt eines Wesens in der jeweils anderen Domäne ist zwar möglich, jedoch nur mit dem Einsatz von starker Magie. Versagt die Magie kollabiert die Struktur, die sich gerade nicht in ihrer Welt befindet und ist unwiderbringlich verloren.

Karma und Psi können die Membran überschreiten und sind in beiden Welten auf die gleiche Weise gültig. Damit man mit einer dieser magischen Kräfte durch die Membran hindurch wirken kann, wird eine extrem hohe Grundfertigkeit benötigt.

========================================
    Die Oberwelt
========================================

(Slogan) Die Welt wie man sie kennt.

Die Oberwelt ist das was wir uns unter einer richtigen Welt verstehen. Hier leben die meisten Bewohner, die Sonne scheint, es gibt Jahreszeiten, Flora und Fauna und viele Völker siedeln. Die Beschreibung der einzelnen Kontinente, ihre Regionen mit der spezifischen Besiedlung und den Charakteren sind das, was jeder Autor sich für seine Geschichten überlegen muss.

Gestirne
####################

Am Himmel über der Oberwelt gibt es Gestirne. Sie bewegen sich in der Regel auf spezifischen Bahnen. Diese müssen keine Kreisbahn sein und die Distanz zur Membran kann schwanken. Die Gestirne sind die Quelle für Licht und Wärme. Ihre Farben und Helligkeiten können variieren.

Um pragmatisch zu bleiben kann für ein Gestirn die Bahn über der Membran festgelegt werden. Sind in der Bahn keine Sprünge oder Unregelmäßigkeiten enthalten, ergebit sich automatisch eine relativ flache Membran. Überfliegt eine Gestirn eine Kuhle in der Membran, kann das Gestirn an mehreren Orten beziehungsweise in einer ganzen Region gleichzeitig im Zenit stehen.

Zusätzlich zur Bahn über dem Boden muss eine Distanz und die Strahlung angegeben werden. Die Strahlung besteht dabei aus Licht oder anderer auch schädlicher Strahlung. Das Licht kann ein bestimmtes Spektrum aufweisen.

Die klassischen Elemente
#############################

Die Obewelt lässt sich grundlegend auch mit den klassischen Elementen Feuer, Erde, Wasser und Luft beschreiben. Dies alles ist prinzipiell überall zu finden ud bildet die Grundlage von allem Leben.

Feuer
====================

Erde
====================

Wasser
====================

Luft
====================

========================================
     Die Unterwelt
========================================

[kein Licht durch Gestirne, Dämonen, Anti-Äther]
