########################################
     Magie
########################################

Es gibt viele verschiedene magische Schulen und Wirkweisen. Teilweise sind sich die Schulen gegenseitig so fremd, dass die Magie nicht gemeinsam oder gegeneinander wirken kann. Die Essenzen der Magie basieren auf den Konzepten Äther (und sein Gegenpart auf der Unterwelt, dem Anti-Äther), Karma und Psi. Insbesondere wenn unterschiedliche Magie aus unterschiedlichen Konzepten aufeinandertrifft, kann die Wirkung sich nicht gegenseitig beeinflussen. Manche Magie wirkt jedoch auf der Basis mehrere Konzepte.

========================================
     Die Basis-Konzepte
========================================

Äther
####################

Alle Dinge dieser Welt sind vom Äther erfüllt. Der Äther representiert alles Stoffliche, jede Materie. Seine Präsenz und Kraft kann von sensiblen Personen gespürt werden. Personen mit der notwendigen Stärke können den Äther auch manipulieren. Wie stark Äther beeinflusst werden kann, hängt von der Ätherkontrolle des Individuums ab. In seltenen Fällen gibt es Ätherkontrolle, obwohl die Person Äther nicht spüren kann. Dann ist die bewusste und gezielte Manipulation nicht möglich. Trotzdem kann sich im Umfeld der Person der Äther verändern und ungeahnte – bisweilen sogar zufällige – Wirkungen zeigen. Die Ätherkontrolle gibt die Kraftreserve an, ähnlich körperlicher Ausdauer. Der persönliche Maximalwert kann durch Training verbessert werden. Wird Äther beeinflusst sinkt der Ätherkontrollwert temporär und erholt sich mit der Zeit automatisch. Bestimmte Rituale, Ruhepausen oder der Aufenthalt an bestimmten Orten kann die Regeneration beschleunigen.

Karma
####################

[Seele; wird lebenslang gesammelt, ein Maximum gibt es nicht; kann als „Kredit bei den Göttern“ verstanden werden; Bei einer Bitte bei einer Gottheit wird das Karma reduziert (und evtl. bei einer anderen Gottheit sogar dadurch verschlechtert); Ist das Karma bei einer Gottheit hoch, wird ein Teil der Kosten auch erlassen (pro 100 Punkte werden Kosten um 1 reduziert, der Malus bei anderen Gottheiten dadurch aber erhöht); eine „Regeneration“ nach der Verwendung gibt es nicht; durch Verschlechterung kann es zu negativen Werten kommen, dann wird die Gottheit zum Widersacher, der sich gelegentlich auch mal rächt (pro 100 Punkte unter Null täglich einmal einen Wurf mit W100 und bei Zutreffen passiert etwas und das Karma wird dadurch wieder etwas besser, oder so)]

Psi
####################
[Konzentration, Kognitivität; Maximale Konzentrationsfähigkeit bestimmt die Möglichkeiten, es gibt leichte Konzentrationsfähigkeit und eine hohe Konzentrationsfähigkeit; bei hoher Konzentration wird die Kraftreserve des Äthers angegriffen, leichte Konzentration ist ohne Folgen und auch dauerhaft möglich]

========================================
     Magische Schulen
========================================

Man mag zwar von Schule sprechen, mitnichten ist jede magiebegabte Person Sprössling mit akademischer Ausbildung. Vielmehr gibt es verschiedene Muster von Zauberern, welche sich in vielen Völkern auf ähnliche Weise finden. Hier sind die Muster beschrieben. Für die detailierte Beschreibung der Zauberer bestimmter Völker muss die Beschreibung des jeweiligen Volkes herangezogen werden. Diese kann sich dann auf die Muster hier beziehen. tabellarische Übersicht mehrere beispielhafter Schulen:

.. csv-table::
    :header: Schule, aktiv nutzbare Konzepte, Wirkungsfelder
    :widths: 1,1,4

    Druiden, "Äther, Karma (Elemente)", "Beeinflussung der Natur, Naturwunder, Tränke und Tinkturen (eingeschränkt)"
    Hexen, "Äther, Psi", "Flüche, Hellsicht und Illusion, Tränke und Tinkturen, Vertrautenmagie"
    Magister, "Äther, Psi", "unterschiedlichste Spruchmagie zur Manipulation von Äther; Illusionen, Hellsicht"
    Elben, "Äther, Psi", "Beeinflussung der Natur, Tränke und Tinkturen (eingeschränkt), Kommunikation, etwas Hellsicht (z.B. Absicht lesen), etwas Illusion (z.B. verstecken und Gedanken verbergen)"
    Priester, Karma, "abhängig von der zugeordneten Gottheit, die erbetene Unterstützung kann gegebenenfalls auch auf Äther und Geist wirken, jedoch liegt die Steuerung nicht direkt in der Hand des Priesters, sondern der Gottheit"
    Seher, Psi, "Hellsicht, mit viel Erfahrung auch Kommunikation und Illusion"
    Schamanen, "Äther, Karma, Psi", "Wie Druide, Priester und Seher zusammen, jedoch in allen Wirkungsfeldern nur manche Möglichkeiten"

Druiden
####################

Druiden sind Wesen mit starker natürlicher Verbindung zu ihrem Lebensraum. Die Verbindung ist in der Regel ausreichend um in die natürlichen Abläufe eingreifen zu können. Druiden gibt es theoretisch in allen Völkern, wobei sie je nach Volk unterschiedliche Fähigkeiten haben können.

Wirkungsfelder und Magiefähigkeiten
========================================

Die möglichen Wirkungsfelder sind Beeinflussung der Natur und Naturwunder. Mit Einschränkung können Druiden Tränke und Tinkturen herstellen. Druiden können Äther sehr gut spüren und beeinflussen. Eremiten halten ihr Tuen und ihr Umfeld im natürlichen Gleichgewicht, wodurch sie in der Lage sind Naturwunder jedes Elementes zu beschwören. Verdingte Druiden (selten) fokussieren sich unter Umständen auf bestimmte Elemente, wodurch sie jedoch negatives Karma anderer Elemente anhäufen und das Gleichgewicht stören können. Psi-Magie ist Druiden nicht bekannt und sie können sich nicht gegen Angriffe auf den Geist schützen.

Leben
====================

[Leben: In der Natur, häufig als Eremit, selten auch im Dienst von Schurken oder den eigenen Vorteil und suchend um Macht zu erlangen]

Arbeiten
====================

[Arbeiten: Pflege einer Landschaft (mit magischer Unterstützung), Gewinnung von stärksten Wirkungsstoffen aus einer Gegend]

Stellung in der Gesellschaft
=================================

[Stellung in der Gesellschaft: unterschiedlich je nach Volk, teilweise geachtet, teilweise sogar verstoßen/geächtet, in der Regel gehören Druiden zu einem Druidenzirkel, welcher auch Personen mit unlauteren Zielen verbannen können]

Hexen
####################

Geht man auf den Markt einer Stadt und sucht sich den Verkaufsstand mit den ausgefallensten Kräutern, Tinkturen und Tränken, so ist es nicht unwahrscheinlich auf Hexen zu stoßen. Diese Männer und Frauen tragen tiefes Wissen und Verständniss über Kräuter und alles was daraus hergestellt werden kann. Sie sind dabei in der Lage Tränke und Tinkturen auch mit geistigen Verbindungen herzustellen.

Wirkungsfelder und Magiefähigkeiten
========================================

Ihre möglichen Wirkungsfelder sind Flüche, Hellsicht und Illusion, Herstellung magischer Tränke und Tinkturen, Vertrautenmagie. Karma-Magie ist Hexen in der Regel nicht bekannt. Hat der Hexer große Mengen Karma gesammelt, kann sich das mit Zutun von Priestern auch auf ihr Tun auswirken.

Leben
====================


Arbeiten
====================


Stellung in der Gesellschaft
=================================


Magister
####################

Wirkungsfelder und Magiefähigkeiten
========================================

[Äther: unterschiedlichste Spruchmagie; keine Karma-Magie; Geist: unterschiedlichste Spruchmagie mit Konzentrationsbedarf, Illusionen, Hellsicht]

Leben
====================


Arbeiten
====================


Stellung in der Gesellschaft
=================================


Elben und andere magische Naturvölker
##########################################

Wirkungsfelder und Magiefähigkeiten
========================================

[Äther: wie Druide; keine Karma-Magie; Geist: Kommunikation mit allen Lebewesen, etwas Hellsicht (Absicht lesen), etwas Illusion (verstecken und Gedanken verbergen)]

Leben
====================


Arbeiten
====================


Stellung in der Gesellschaft
=================================


Priester
####################

Wirkungsfelder und Magiefähigkeiten
========================================

[Nur Karma-Magie: abhängig von der zugeordneten Gottheit, die erbetene Unterstützung kann gegebenenfalls auch auf Äther und Geist wirken, jedoch liegt die Steuerung nicht direkt in der Hand des Priesters, sondern der Gottheit]

Leben
====================


Arbeiten
====================


Stellung in der Gesellschaft
=================================


Seher
####################

Wirkungsfelder und Magiefähigkeiten
========================================

[Nur Geist-Magie aktiv: Hellsicht, mit viel Erfahrung auch Kommunikation und Illusion; Äther: passive Fähigkeit zur Unterstützung der Geist-Magie; keine Karma-Magie]

Leben
====================


Arbeiten
====================


Stellung in der Gesellschaft
=================================


Schamanen
####################

Wirkungsfelder und Magiefähigkeiten
========================================

[Äther-Magie wie Druiden; Karma-Magie wie Priester; Geist-Magie wie Seher; jedoch in allen Ebenen nur manche Handlungsmöglichkeiten]

Leben
====================


Arbeiten
====================


Stellung in der Gesellschaft
=================================


========================================
     Wirkungsfelder
========================================

Jeder Magiebegabte verfügt über sogenannte Wirkungsfelder. Diese sind ihm nicht unbedingt als solche bekannt. Ein Wirkungsfeld hängt immer von bestimmten Konzepten ab und kann nur von Begabten der jeweiligen Konzepte genutzt werden. Im Wirkungsfeld gibt es nochmals verschiedenen Möglichkeiten Magie zu wirken. Konkrete Zauber sind in der Zauberliste aufgeführt.

Beeinflussung der Natur
############################

Bewussten Einfluss auf die Natur erfordert die Fähigkeit Äther zu spüren. Jeder Einfluss auf die Natur erfordert ein bestimmtes Maß an Ätherkontrolle. Im Allgemeinen gehört zu der Beeinflussung jede Veränderung von Materie. Die möglichen Zauber sind daher auch nach Elementen gruppiert. Bei der Veränderung der Natur ist zu beachten, dass dies in der Regel einen Einfluss auf das Karma der Elemente und möglicherweise auch von Gottheiten hat.

Flüche
####################

Wer sich mit Hexen anlegt, der wird sehr wahrscheinlich den Fluch der Hexen spüren. Ist man mit Flüchen von Hexen belegt, sind Geist und Äther so manipuliert, dass bestimmte Eigenschaften des Äthers für Veränderungen am eigenen Körper führen. Dabei bilden sich zwischen Geist von Hexe und Opfer dauerhafte Verbindungen, wodurch die Hexe jederzeit teilweise Zugriff auf den Körper des Opfers hat. Um den Fluch zu beenden muss die geistige Verbindung gelöst werden.

Hellsicht und Illusion
###########################

Geht es darum etwas zu sehen, was nicht sichtbar ist, gibt es nur die Möglichkeit die stoffliche Ebene zu verlassen und auf der geistigen Ebene zu suchen. Zauberer mit starkem Geist vermögen es, unsichtbares oder weit entferntes zu erkennen. So bleiben auch Absichten nicht verborgen und die wahre Gestalt des Gestaltenwandlers wird erkannt. Auch die Verbindung zu den Ahnen gehört in dieses Wirkungsfeld. Und wer etwas verbergen möchte wirkt seinen Zauber ebenfalls auf der geistigen Ebene. So wird die eigene Absicht verschleiert oder sogar das Aussehen in seiner Wahrnehmung verändert. Wer die Verbindugn zu den Ahnen nicht nur spüren, sondern auch zur Erscheinung bringen will benötigt ebenfalls illusorische Fähigkeiten. Diese beiden Wirkungsfelder sind direkte Gegenspieler, weshalb sie hier zusammen erläutert werden. Viele Zauber welche als Angriff ausgeführt werden, können mit dem richtigen Gegenzauber abgeblockt oder fehlgeleitet werden.

Herstellung magischer Tränke und Tinkturen
###############################################

Was ein echter Zaubertrank ist, wurde nicht nur in der Richtigen Reihenfolge bei exakt korrekter Handhabung zusammengerührt. Es kommt auch darauf an den Äther der Bestandteile zu beeinflussen, sodass die richtige Wirkung entsteht. Nebenwirkungen werden so ausgeschaltet und die gewünschte Wirkung hervorgehoben. Ätherkontrolle ist daher für alle Tränke und Tinkturen unabdingbar. Für manche Wirkungen müssen geistige Verbindungen zwischen dem Erzeugnis und Individuen geflochten werden. Zum Beispiel wirkt der Liebestrank nicht nur betörend, sondern bindet die Personen fest zusammen, wenn beide ihren Teil davon trinken. Tränke und Tinkturen mit geistiger Komponente können nur hergestellt werden, wenn Psi-Magie aktiv ausgeführt werden kann.

Naturwunder
####################

Wenn es nicht reicht, die Natur über den Äther zu beeinflussen, müssen evtl. Naturwunder beschworen werden. Man spricht von Wunder, da es sich nicht um direktes Tun des Zaubernden handelt, sondern die Veränderung von Gottheiten gewirkt werden. Dabei kann auch schon mal über das Ziel hinaus geschoßen werden, wenn die Gottheit mit zu viel Enthusiasmus seinen treuen Dienern unter die Arme greifen will. So kann die Bitte nach etwas Regen durch Wüstenbewohner auch mal zur Sintflut werden.

Naturwunder sind Bitten an eine Gottheit. Siehe dazu auch Wunder im Allgemeinen.

Vertrautenmagie
####################

Da läuft sie wieder. Die schwarze Katze die Unglück bringt. Aberglaube? Weit gefehlt, denn es ist nicht die beliebige schwarze Katze, sondern die Katze von der Kräuterfrau vom Markt. Und weil die Kinder sie wiedereinmal geärgert haben passiert, was passieren muss. Beim Toben und Tollen fällt der Kleinste in die große Jauchegrube und konnte gerade im letzten Moment noch gerettet werden. Jetzt hat er überall häßlichen Ausschlag und nur besagt Kräuterfrau kann helfen. Kein Zufall, sondern der kleine Racheakt der Kräuterfrau. Sie hat ihre Katze entsendet um genau dies geschehen zu lassen. Mit der Psi sind Katze und Hexe eng verbunden. Der Vertraute ist Auge, Ohr und Bote der Hexe.

Wunder im Allgemeinen
##########################

Für außergewöhnliche Unterstützung kann eine Gottheit um ein Wunder gebeten werden. Um Wunder zu bitten ist Individuen vorbehalten, welche diese Eigenschaft aktiv besitzen. Sie verwenden dazu das Karma, welches sie bei der jeweiligen Gottheit haben. Werden wirklich große Wunder benötigt, können gegebenenfalls Rituale abgehandelt werden um Karma anderer Personen einzuschließen. Diese müssen dann auch nicht unbedingt die aktive Karma-Fähigkeit besitzen.
